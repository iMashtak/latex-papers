\section{Особенности реализации многослойной архитектуры backend}

Поэтапно рассмотрим, каким образом разрабатывается система в рамках выбранной архитектуры.

Первый этап -- это фиксирование модели предметной области (набора сущностей и отношений между ними) в виде классов и ассоциированных с ними методов. Эти классы будут расположены в доменном слое в отдельном .NET-проекте. Будем называть такой проект ограниченным контекстом (по DDD -- bounded context).

При этом на классы накладываются следующие ограничения:

\begin{compactenum}
    \item[1)] классы должны иметь поле \texttt{Id}, при помощи которого производится идентификация всех сущностей в системе;
    \item[2)] объекты доменного уровня должны создаваться только в корректном состоянии, то есть их конструктор должен принимать такое количество параметров, которого хватит для инициализации всех своих полей;
    \item[3)] объекты должны быть неизменяемыми, любые изменения значений полей объекта должны осуществляться через некоторые методы репозитория, ассоциированного с рассматриваемым ограниченным контекстом.
\end{compactenum}

Под репозиторием подразумевается интерфейс-синглтон, содержащий фиксированный набор действий над хранилищем данных. Если требуется изменить значение некоторого поля, то следует добавить в этот интерфейс метод по изменению этого поля и возвращению корректного значения. Самостоятельно изменять значение поля не рекомендуется (возможно только в тривиальных случаях).

\begin{lstlisting}[label=Listing:Example:Artifact,caption={Пример реализации доменного класса Artifact}]
public class Artifact : DomainObject {
    public ArtifactType        Type       { get; }
    public string              Template   { get; }
    public int                 InstanceId { get; }
    public string              Alias      { get; }
    public IEnumerable<string> States     { get; private set; }
    public void AddState(string state) { 
        this.States = IProcessesRepository.Impl
            .AddArtifactState(this.Id, state); 
    }
    public void RemoveState(string state) {
        this.States = IProcessesRepository.Impl
            .RemoveArtifactState(this.Id, state);
    }
    public Artifact(
        int id, ArtifactType type, string template,
        int instanceId, string alias, IEnumerable<string> states
    ) : base(id) {
        this.Type = type;
        this.Template = template;
        this.InstanceId = instanceId;
        this.Alias = alias;
        this.States = states;
    }
}
\end{lstlisting}

На листинге \ref{Listing:Example:Artifact} приведён пример реализации класса доменного слоя --- артефакта. В конструктор передаются значения всех полей и определено два метода по изменению значений поля \texttt{States}. Как указывалось в требовании 3 выше, новое значение полю \texttt{States} присваивается как возвращаемое значение из метода репозитория, а не вычисляется самостоятельно.

Второй этап -- фиксирование набора сценариев использования системы в виде методов слоя приложения. Здесь важно правильно отделять логику взаимоотношений сущностей предметной области от способа использования этих отношений в рамках системы для достижения целей системы.

Например, сценарий использования может проверять корректность команды, поступившей от пользователя, его права, возможность выполнять сценарий, изменять результаты работы методов доменного слоя в зависимости от прав пользователя. То есть задача слоя приложения -- получить объекты доменного слоя из базы данных и вызвать их методы в нужном для достижения целей сценария порядке.

Для реализации сценариев использования могут потребоваться дополнительные методы доступа к данным -- получение, удаление, создание. В связи с этим набор методов интерфейса репозитория расширяется.

Третий этап -- реализация инфраструктурного слоя. Сначала разрабатываются классы, задающие схему базы данных (посредством технологии ORM: Entity Framework Core), а затем реализуются репозитории, разрабатываются запросы к базе данных. На этом этапе важным аспектом является определение преобразования объектов доменного слоя в соответствующие объекты инфраструктурного слоя (и наоборот). Так как на объекты инфраструктурного слоя накладывается достаточно большое количество ограничений, то иногда это является нетривиальной задачей.

Для осуществления таких преобразований используются несколько специальных программных механизмов. Первый -- использование кэша преобразованных объектов. Для этого реализованы классы EntityToDomainCache (листинг \ref{Listing:Example:EntityToDomainCache}) и DomainToEntityCache (листинг \ref{Listing:Example:DomainToEntityCache}). Эти классы сохраняют в себе все объекты, участвующие в преобразовании, а также сопоставляют одни объекты другим. Так решается проблема преобразования классов с циклическими ссылками -- если преобразуемый объект уже преобразовывался раньше, то преобразование не нужно осуществлять и достаточно вернуть сохранённый объект из кэша. 

Однако, исходя из требований к структуре доменных объектов, эти объекты должны целиком инициализироваться сразу на этапе вызова конструктора. Поэтому кажется невозможным разрешить циклические ссылки при преобразовании объектов инфраструктурного слоя в объекты доменного. Тогда применяется второй механизм -- оборачивание типов полей доменных классов в класс Loadable (листинг \ref{Listing:Example:Loadable}). Этот класс выполняет роль контейнера значения, в который можно положить значение единожды. Помимо отражения семантики полноты инициализации объекта данными из базы, класс Loadable помогает разрешить проблему преобразования данных с циклическими ссылками.

Наконец, последний, четвёртый этап -- это реализация слоя представления или декларирование API. В терминах фреймворка ASP.NET Core классы-обработчики HTTP-запросов называются контроллерами. Строгое требование к методам контроллеров -- их когнитивная сложность должна равняться единице.

Когнитивная сложность фрагмента кода -- это характеристика, показывающая, насколько много путей исполнения кода имеется в данном фрагменте. Когнитивная сложность, равная единице, означает, что фрагмент кода не имеет ни одного if-выражения, ни одного цикла. То есть единственное предназначение контроллеров -- преобразовать данные из пришедшего запроса в аргументы соответствующего метода слоя приложения.