\section{Разработка языка структурного описания процесса}\label{Section:Theory:Lang}

В качестве базового синтаксиса для языка структурного описания процесса будем использовать YAML.

На листинге \ref{Listing:Lang:YAMLRoot} представлена верхнеуровневая структура для определений структуры процесса. Подразумевается, что подобная структура будет использоваться для конструирования не только процессов, но и других объектов, рассматриваемых в рамках разрабатываемой системы управления НИР. 

\begin{lstlisting}[label=Listing:Lang:YAMLRoot,caption={Верхнеуровневая структура YAML-определения},language=make]
namespace: <string>
type: Process 
template: <string>
process: <key:value>
\end{lstlisting}

Верхнеуровневое описание включает в себя определение пространства имён, в котором существует данное определение (ключ \texttt{namespace}), тип описываемого объекта (ключ \texttt{type}) и название шаблона (ключ \texttt{template}). Под шаблоном понимается структурное описание процесса. Типы объектов определяются на уровне системы, то есть, если указанный в этом разделе тип не будет найден в системе, то данная ситуация будет признана ошибочной. В то же время, пространства имён и названия шаблонов задаются произвольно с тем ограничением, что все названия пространств имён в рамках системы должны быть уникальны, а названия шаблонов должны сохранять уникальность в рамках своего пространства имён.

Как следует из листинга \ref{Listing:Lang:ProcessAttributes}, процесс структурно описывается n-кой, в которую входят множество артефактов, множество дат, множество возможных в рамках процесса событий, множество стадий и множество связей. Используется запись, при которой в угловых скобках указывается тип значения для данного ключа.

\begin{lstlisting}[label=Listing:Lang:ProcessAttributes,caption={Синтаксис описания составляющих частей процесса},language=make]
process:
    artifacts: <key:value>
    dates: <key:value>
    events: <key:value>
    stages: <key:value> 
    gates: <key:value>    
\end{lstlisting}

Рассмотрим каждый компонент отдельно. На листинге \ref{Listing:Lang:ArtifactAttibutes} представлен пример определения артефакта с псевдонимом \texttt{artifactOne}, для которого определяются тип артефакта и используемый в рамках этого типа шаблон.

\begin{lstlisting}[label=Listing:Lang:ArtifactAttibutes,caption={Синтаксис описания артефактов процесса},language=make]
artifacts:
    artifactOne: 
        type: <string>
        template: <string>
\end{lstlisting}

Даты представляются при описании процесса с использованием псевдонимов. Это сделано для того, чтобы имелась возможность осуществлять 3 вида преобразований над датами: статическую подстановку, динамическую подстановку и применение операций. На листинге \ref{Listing:Lang:DatesAttributes} представлен синтаксис для этих преобразований соответственно на 4й, 5й и 6й строках.

\begin{lstlisting}[label=Listing:Lang:DatesAttributes,caption={Синтаксис описания дат, используемых в рамках процесса},language=make]
dates:
    dateOne: 2020.12.14
    dateTwo: 2020.12.21
    >>dateStaticSubstitution: -
    |>dateDynamicSubstitution: -
    dateDepends: dateTwo + 2 weeks
\end{lstlisting}

Рассмотрим подробнее эти преобразования. В рамках текущей версии модели процессов эта преобразования допускаются только для дат, хотя ничто не мешает продумать их использование и для других сущностей.

\begin{compactitem}
    \item \textit{Статическая подстановка} значения ключа показывает, что конкретное значение этому ключу будет присвоено в момент конструирования конкретного процесса по описываемому шаблону. Обозначается добавлением символов \text{>>} к ключу. 
    \item \textit{Динамическая подстановка} значения ключа показывает, что конкретное значение этому ключу будет присвоено в процессе исполнения процесса, то есть может быть неизвестным на момент конструирования. Обозначается дополнением символов \texttt{|>} к ключу.
    \item \textit{Применение операций} позволяет вычислять значения одних ключей на основе значений других ключей, то есть делать значения зависимыми. Для дат определяется операция \texttt{+}, позволяющая к дате прибавить некоторый произвольный интервал времени (в примере к дате, имеющей псевдоним \texttt{dateTwo} прибавляется интервал длиной две недели). Заметим, что если в выражении с операциями участвуют даты, для которых записано определённое значение (как в примере), то можно считать, что это выражение уже вычислено. Если в выражении используются ключи, для которых определена статическая подстановка, то и определяемый ключ будет являться статической подстановкой. Аналогично для случая динамической подстановки. При этом, если в выражении используются одновременно и статические, и динамические подстановки, то определяемый ключ будет иметь динамическую подстановку.
\end{compactitem}

Синтаксис определения возможных событий в рамках процесса приведён на листинге \ref{Listing:Listing:Lang:EventsAttributes}. В угловых скобках указан список альтернатив: возможных значений для рассматриваемых ключей. Смысл этих значений совпадает с тем описанием, что даётся в разделе \ref{Subsection:Theory:SemanticModel:Conceptual}.

\begin{lstlisting}[label=Listing:Listing:Lang:EventsAttributes,caption={Синтаксис описания событий системы},language=make]
events:
    type: <Single | Multiple>
    destination: <Environment | Process>
    kind: <Start | Intermediate | End>
\end{lstlisting}

Стадии описываются аналогичным с датами и артефактами способом: через псевдонимы. На листинге \ref{Listing:Lang:StagesAttributes} представлена пример описания стадии. Обозначение \texttt{array of} показывает, что значением данного ключа является набор значений определённого вида. Определяется имя стадии \texttt{name}, дата в виде псевдонима даты \texttt{date}, список ролей, которыми может обладать участник процесса, чтобы сгенерировать событие $\langle done_s \rangle$ для данной стадии, аргументы и результаты стадии.

\begin{lstlisting}[label=Listing:Lang:StagesAttributes,caption={Синтаксис описания стадий процесса},language=make]
stages:
    stageOne:
        name: <string>
        date: <date alias>
        roles: <array of strings>
        arguments: <array of artifact aliases>
        results: <array of artifact aliases>
        pre: <array of key:value>
        post: <array of key:value>
\end{lstlisting}

Подробно остановимся на описании пре- и постусловий и операций стадии. Как отмечалось в разделе \ref{Subsection:Theory:SemanticModel:Conceptual}, для стадии определяется набор таких пар из условия и операции. Синтаксис их описания представлен на листинге \ref{Listing:Lang:PrePostConditionOperationAttributes}.

\begin{lstlisting}[label=Listing:Lang:PrePostConditionOperationAttributes,caption={Синтаксис описания пре- и постусловий и операций},language=make]
pre:
    - condition: <condition syntax>
      operation: <operation syntax>
    - ... 
post: ... 
\end{lstlisting}

Для описания условий и операций недостаточно выразительных средств языка YAML. Поэтому возникает потребность разработать собственный Domain Specific Language (DSL). Для этого разработано две грамматики, представленные на листингах \ref{Listing:Lang:ConditionSyntaxBNF} и \ref{Listing:Lang:OperationSyntaxBNF}. 

\begin{lstlisting}[label=Listing:Lang:ConditionSyntaxBNF,caption={Грамматика языка описания пре и постусловия стадии, представленная в БНФ},language=XML]
<condition> ::= <expr>
<expr> ::= <none-expr> | <not-expr> | <and-expr> | 
           <or-expr> | <option> | "(" <expr> ")"
<none-expr> ::= "none"
<and-expr> ::= <expr> "and" <expr>
<or-expr> ::= <expr> "or" <expr>
<not-expr> ::= "not" <expr>
<option> ::= <state-check> | <event-check>
<state-check> ::= <literal> "at" <literal>
<event-check> ::= "<" <literal> ">"
\end{lstlisting}

\begin{lstlisting}[label=Listing:Lang:OperationSyntaxBNF,caption={Грамматика языка описания пре и постоперации стадии, представленная в БНФ},language=XML]
<operation> ::= <statement> | (<operation> ";" <statement>)+
<statement> ::= <add-state> | <remove-state>
<add-state> ::= <literal> "+=" <literal>
<remove-state> ::= <literal> "-=" <literal>
\end{lstlisting}

Последней частью процесса является множество связей. Способ из задания указан на листинге \ref{Listing:Lang:GatesAttributes}. Имеется два варианта: связь с типом соединения $1 \to 1$ указывается так (ключ \texttt{gateOne}) и связь с любыми другими типами соединения (ключ \texttt{gateTwo}).

\begin{lstlisting}[label=Listing:Lang:GatesAttributes,caption={Синтаксис описания связей между узлами процесса},language=make]
gates:
    gateOne:
        from: [<stage or gate alias>]
        to: [<stage or gate alias>]
    gateTwo:
        from: <array of stage and gate aliases>
        to: <array of stage and gate aliases>
        type: <All | One | Any | Async>
\end{lstlisting}

Используя приведённый язык представляется возможным конструировать процессы в соответствии с семантической моделью процессов (раздел \ref{Section:Theory:SemanticModel}).