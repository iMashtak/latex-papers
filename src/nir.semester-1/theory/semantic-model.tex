\section{Разработка семантической модели процесса}\label{Section:Theory:SemanticModel}

В рамках данной работы ранняя семантическая модель процессов подвергается основательной переработке. Ранняя модель была призвана сочетать в себе возможности BPMN, при этом оставаться понятной и простой как eEPC. Для достижения более широких выразительных возможностей, а значит для возможности более чётко и корректно отражать семантику конкретного процесса, производится внедрение принципов инверсии зависимостей, а также определение точек внедрения внешних элементов. Семантическая модель понимается как объединение концептуальной модели и принципа исполнения процесса. Эти два аспекта раскрываются в последующих разделах.

%%-----------------------------------------------

\subsection{Концептуальная модель процесса}\label{Subsection:Theory:SemanticModel:Conceptual}

На рисунке \ref{Picture:SemanticProcessModel} представлена концептуальная модель процесса в виде Entity relationship диаграммы. Последовательно опишем все присутствующие на диаграмме сущности и отношения.

\begin{figure}[ht]
    \begin{center}
        \includegraphics[width=.9\linewidth]{\pwd/img/SemanticProcessModel.png}
    \end{center}
    \caption{Концептуальная модель процессов, представленная в виде ER-диаграммы}
    \label{Picture:SemanticProcessModel}
\end{figure}

Центральное место занимает сущность \textbf{Process}, которая является агрегатом из всех остальных сущностей, представленных на диаграмме. Для процесса в качестве атрибутов определяется название шаблона, по которому был создан этот процесс, состояние процесса (Created - созданный и инициализированный процесс, InProgress - запущенный процесс, Completed - завершённый процесс), а также даты. Даты представляются как словарь значений в связи с использованием механизма подстановок при описании процесса, о котором подробнее рассказывается в разделе \ref{Section:Theory:Lang} (листинг \ref{Listing:Lang:DatesAttributes}). 

Для процесса определяется (\textit{Uses}) набор событий \textbf{Event}, каждое из которых имеет следующие атрибуты: название Name, тип Type (Single -- происходит один раз в течение исполнения процесса, Multiple -- может происходить несколько раз), место происхождения Destination (Environment -- из окружения процесса, Process -- из самого процесса), вид Kind (Start -- начальное, Intermediate -- промежуточное, End -- конечное) и флаг о том, что событие произошло Occured.
\begin{compactitem}
    \item Если установлен тип события как Single, то флаг Occured устанавливается в true, когда событие происходит, и остаётся в таком положении до конца. Если же тип события -- Multiple, то флаг сбрасывается в false каждый раз, когда завершается процедура передачи активности (раздел \ref{Subsection:Theory:SemanticModel:Rules}).
    \item События с местом происхождения Environment могут совершаться по команде от пользователя системы. События с местом происхождения Process могут генерироваться только в пре- и постоперациях стадий процесса.
\end{compactitem}

Процесс состоит (\textit{Consists of}) из стадий \textbf{Stage} и имеет (\textit{Declares}) набор связей \textbf{Gate}.

Стадии имеют некоторый идентификатор, дату завершения (точнее, псевдоним даты, имеющийся среди дат, определённых для процесса в целом) и состояние (Open -- начальное состояние стадии, InProgress -- стадия в процессе выполнения, Done -- стадия завершена, Closed -- стадия закрыта). Для стадии определён \textit{Managed by} список ролей \textbf{Role}. С точки зрения концептуальной модели роль имеет только имя. Также стадия имеет набор пред- и постусловий \textbf{Condition} и операций \textbf{Operation}, которые связаны отношениями \textit{Is preprocessing} и \textit{Is postprocessing}. Эти условия и операции объединены в пары <<условие-операция>>. Общее правило состоит в том, что предусловие проверяется в тот момент, когда стадию переводят в состояние InProgress, а постусловие -- когда стадию переводят в состояние Done. Если условие при проверке оказалось истинным, то сразу же выполняется связанная с ним операция. Эти правила формально описаны в разделе \ref{Subsection:Theory:SemanticModel:Rules}.

Связи характеризуются типом Type (None, One, All, Any, Async) и состоянием State (Open, Active, Closed). Подробно о типах и состояниях связей написано в разделе \ref{Subsection:Theory:SemanticModel:Nodes}. Связи соединяют между собой узлы \textbf{Node} процесса. При этом связи обязательно направленные за счёт наличия двух отношений: \textit{From} и \textit{To}. Считается, что связь задаёт направление исполнения процесса от узлов, состоящих в отношении \textit{From} с рассматриваемой связью, к узлам, стоящих в отношении \textit{To} с рассматриваемой связью.

Узлами являются как стадии, так и связи (что выражено через отношение \textit{Is a}).

В рамках концептуальной модели определяются артефакты \textbf{Artifact} процесса, которыми могут быть произвольные объекты системы. Для каждого артефакта определяется тип Type, шаблон конструирования артефакта Template (по аналогии с шаблоном процесса, подробнее в разделе \ref{Section:Theory:Lang}), ссылки на конкретный объект InstanceRef, псевдоним артефакта Alias (использованный в структурном описании процесса) и набор состояний артефакта States. Считается, что артефакту можно присваивать различные состояния (не только одно), при помощи которых можно управлять исполнением процесса. Состояния артефактов могут использоваться в пред- и постусловиях и операциях стадий процесса. Соответственно, чтобы это было возможным делать, для стадии артефакты рассматриваются как аргументы (\textit{Is argument}) или как результаты (\textit{Is result}). Состояние аргументов возможно получить, но не изменить, а состояние результатов - как получить, так и изменить.

%%-----------------------------------------------

\subsection{Состояния, типы связей и соединений узлов процесса}\label{Subsection:Theory:SemanticModel:Nodes}

Исходя из построенной модели, выделяется 4 состояния узла, 3 типа связей и 3 способа соединения узлов. Опишем подробно эти возможности:

\begin{compactitem}
    \item Состояния узлов: 
    \begin{compactenum}
        \item \textit{Open} --- начальное состояние каждого узла.
        \item \textit{InProgress} --- состояние, обозначающее, что данный узел сейчас активен и для продолжения работы процесса необходимо его перевести в состояние \textit{Done} (завершился).
        \item \textit{Done} --- состояние, обозначающее, что данный узел был активным, но только что завершился.
        \item \textit{Closed} --- конечное состояние узла.
    \end{compactenum}
    Состояние любого узла последовательно переходит от \textit{Open} к \textit{Closed}. В рамках семантической модели процесса возможно возникновение циклов; в таком случае состояние \textit{Closed} сменится на состояние \textit{InProgress}.

    \item Способы соединения узлов:
    \begin{compactenum}
        \item $1 \to 1$ --- простая последовательная связь двух узлов между собой. Тип связи не требуется.
        \item $1 \to N$ --- связь вида "один ко многим". Требует для себя указания типа связи.
        \item $N \to 1$ --- связь вида "многие к одному". Требует для себя указания типа связи.
    \end{compactenum}
    Соединение вида $N \to M$ не вводится в качестве базисного, так как оно реализуется в виде последовательного использования соединений $N \to 1$ и $1 \to M$. В этом случае потребуется промежуточная связь. 

    \item Типы связей:
    \begin{compactenum}
        \item $\text{AND}(1 \to N)$ --- запускает $N$ узлов одновременно и параллельно, дожидаясь возможности запуска каждого из этих узлов.
        \item $\text{AND}(N \to 1)$ --- ожидает завершения $N$ узлов и только после этого запускает исходящий узел.
        \item $\text{XOR}(1 \to N)$ --- из имеющихся на выходе $N$ узлов выбирает первый, который возможно завершить, и запускает только его.
        \item $\text{XOR}(N \to 1)$ --- из имеющихся на входе $N$ узлов ищет первый завершённый, после чего завершает только его и запускает исходящий узел.
        \item $\text{OR}(1 \to N)$ --- из имеющихся на выходе $N$ узлов выбираются те, которые возможно запустить и они запускаются. Так может быть запущено произвольное количество узлов, но хотя бы один.
        \item $\text{OR}(N \to 1)$ --- из имеющихся на входе $N$ узлов выбираются те, которые можно завершить, и они завершаются. Так может быть завершено произвольное количество узлов, но хотя бы один.
        \item $\text{ASYNC}(1 \to N)$ --- по мере того, как становится возможным запустить некоторый из $N$ узлов, он запускается.
        \item $\text{ASYNC}(N \to 1)$ --- по мере того, как становится возможным завершить некоторый из $N$ узлов, он завершается.
    \end{compactenum}
    Принципиальным отличием типов связей $\text{AND}$, $\text{XOR}$, $\text{OR}$ от типа связи $\text{ASYNC}$ является то, что связь, имеющая тип $\text{ASYNC}$, не может быть переведена в состояние завершённой. Такие связи позволяют организовывать событийно-ориентированное исполнение процесса в случае, если это необходимо.
\end{compactitem}

Тип связи $\text{ASYNC}$ позволяет реализовывать в рамках модели процессов такую концепцию, как шина событий (шина сообщений) \cite{Study:Words-About-Event-Bus}. 

%%-----------------------------------------------

\subsection{Описание правил исполнения процесса}\label{Subsection:Theory:SemanticModel:Rules}

При описании правил исполнения процесса будем использовать следующие принципы и обозначения:

\begin{compactitem}
    \item Правила записываются в виде двух выражений, разделённых горизонтальной чертой. Сверху записывается набор условий, а снизу - набор действий. При выполнении всех условий выполняются все действия.
    \item Множество элементов типа $T$ обозначается как $\{T\}$.
    \item Знак $=$ обозначает оператор сравнения на эквивалентность.
    \item Знак $:=$ обозначает операцию присвоения.
    \item Конструкция $object.Attribute$ обозначает доступ к значению атрибута $Atribute$ объекта $object$.
    \item Конструкция $\langle event \rangle$ обозначает требования свершения события $event$ в рамках исполнения процесса. Если используется в условии правила, то проверяется свершение события. Если используется в действии, то событие совершается.
    \item Конструкция $o:Object$ описывает отношение типизации, объект $o$ имеет тип $Object$.
    \item Конструкция $\exists \{os\} (condition)$ означает требование существования некоторого подмножества, для каждого элемента $os$ которого выполняется условие $condition$.
\end{compactitem}

Также для краткости примем некоторые умолчания об именах объектов. Пусть имена объектов начинаются с той же буквы, что начинается их тип, но только не с заглавной, а с прописной. То есть $p:Process$, $n:Node$, $g:Gate$, $s:Stage$, $e:Event$.

\textbf{Инициализация процесса $p$.} Правило описывает состояние процесса на момент его создания.
\begin{equation}\label{Rule:ProcessInit}
    \frac{
        \langle init \rangle
    }{
        \forall s \in p.Stages (s.State := Open) ;\; p.State := Created
    }
\end{equation}

\textbf{Запуск процесса $p$.} Для процесса обязательно определено стандартное событие $\langle start \rangle$, обозначающее старт процесса. Вследствие этого явным образом <<начальный>> узел процесса не выделяется, вместо этого подразумевается, что требование свершения события $\langle start \rangle$ указано в предусловиях каких-либо стадий.
\begin{equation}\label{Rule:ProcessStart}
    \frac{
        p.Status = Created
    }{
        \langle start \rangle ;\; p.Status := InProgress
    }
\end{equation}

\textbf{Вырожденное состояние узла-связи $g$.} Так как для любого узла устанавливается 4 возможных состояния, описанных в разделе \ref{Subsection:Theory:SemanticModel:Nodes}, однако состояние \textit{InProgress} для связи не имеет смысла (ведь не существует понятия <<выполнение связи>>). В связи с этим это состояние сразу переводится в состояние \textit{Done}.
\begin{equation}\label{Rule:GateUselessState}
    \frac{
        g.State = InProgress
    }{
        g.State := Done
    }
\end{equation}

\textbf{Завершение стадии $s$.} Для завершения стадии в процесс неявно помещается событие $\langle done_s \rangle$, которое обрабатывается в соответствии с правилом.
\begin{equation}\label{Rule:StageDone}
    \frac{
        s.State = InProgress \land \langle done_s \rangle \land s.PostCondition
    }{
        s.State := Done ;\; s.PostOperation
    }
\end{equation}

\textbf{Передача активности для связи $g$ с типом соединения $1 \to 1$.} Формально этот случай описывается следующим условием: $g.From : Node$, $g.To : Node$;
\begin{equation}\label{Rule:ActivityForwardSimpleGate}
    \frac{
        g.From.State = Done \land g.To.State = (Open \lor Closed) \land g.To.PreCondition
    }{
        g.From.State := Closed ;\; g.To.State := InProgress ;\; g.To.PreOperation
    }
\end{equation}

\textbf{Передача активности для связи $g$ типа AND с типом соединения $1 \to N$.} Формально этот случай описывается следующим условием: $g.From : Node$, $g.To : \{Node\}$, $g.Type = And$.
\begin{equation}\label{Rule:ActivityForwardManyOutputsAnd}
    \frac{
        g.From.State = Done \land \forall n \in g.To (n.State = (Open \lor Closed) \land n.PreCondition)
    }{
        g.From.State := Closed ;\; \forall n \in g.To (n.State := InProgress ;\; n.PreOperation)
    }
\end{equation}

\textbf{Передача активности для связи $g$ типа AND с типом соединения $N \to 1$.} Формально этот случай описывается следующим условием: $g.From : \{Node\}$, $g.To : Node$. $g.Type = And$.
\begin{equation}\label{Rule:ActivityForwardManyInputsAnd}
    \frac{
        \forall n \in g.From (n.State = Done) \land g.To.State = (Open \lor Closed) \land g.To.PreCondition
    }{
        \forall n \in g.From (n.State := Closed) ;\; g.To.State := InProgress ;\; g.To.PreOperation
    }
\end{equation}

\textbf{Передача активности для связи $g$ типа XOR с типом соединения $1 \to N$.} Формально этот случай описывается следующим условием: $g.From : Node$, $g.To : \{Node\}$, $g.Type = Xor$.
\begin{equation}\label{Rule:ActivityForwardManyOutputsXor}
    \frac{
        g.From.State = Done \land \exists! n \in g.To (n.State = (Open \lor Closed) \land n.PreCondition)
    }{
        g.From.State := Closed ;\; n.State := InProgress ;\; n.PreOperation
    }
\end{equation}

\textbf{Передача активности для связи $g$ типа XOR с типом соединения $N \to 1$.} Формально этот случай описывается следующим условием: $g.From : \{Node\}$, $g.To : Node$. $g.Type = Xor$.
\begin{equation}\label{Rule:ActivityForwardManyInputsXor}
    \frac{
        \exists! n \in g.From (n.State = Done) \land g.To.State = (Open \lor Closed) \land g.To.PreCondition
    }{
        n.State := Closed ;\; g.To.State := InProgress ;\; g.To.PreOperation
    }
\end{equation}

\textbf{Передача активности для связи $g$ типа OR с типом соединения $1 \to N$.} Формально этот случай описывается следующим условием: $g.From : Node$, $g.To : \{Node\}$, $g.Type = Or$.
\begin{equation}\label{Rule:ActivityForwardManyOutputsOr}
    \frac{
        g.From.State = Done \land \exists \{ns\} \in g.To (ns.State = (Open \lor Closed) \land ns.PreCondition)
    }{
        g.From.State := Closed ;\; \forall n \in \{ns\} (n.State := InProgress ;\; n.PreCondition)
    }
\end{equation}

\textbf{Передача активности для связи $g$ типа OR с типом соединения $N \to 1$.} Формально этот случай описывается следующим условием: $g.From : \{Node\}$, $g.To : Node$. $g.Type = Or$.
\begin{equation}\label{Rule:ActivityForwardManyInputsOr}
    \frac{
        \exists \{ns\} \in g.From (ns.State = Done) \land g.To.State = (Open \lor Closed) \land g.To.PreCondition
    }{
        \forall n \in ns (s.State := Closed) ;\; g.To.State := InProgress ;\; g.To.PreOperation
    }
\end{equation}

\textbf{Передача активности для связи $g$ типа ASYNC с типом соединения $1 \to N$.} Формально этот случай описывается следующим условием: $g.From : Node$, $g.To : \{Node\}$, $g.Type = Async$.
\begin{equation}\label{Rule:ActivityForwardManyOutputsAsync}
    \frac{
        \begin{matrix}
            g.From.State = (Done \lor Closed) \land \\ \land \exists \{ns\} \in g.To (ns.State = (Open \lor Closed) \land ns.PreCondition)
        \end{matrix}
    }{
        g.From.State = Closed ;\; \forall n \in ns (n.State := InProgress \land n.PreOperation)
    }
\end{equation}

\textbf{Передача активности для связи $g$ типа ASYNC с типом соединения $N \to 1$.} Формально этот случай описывается следующим условием: $g.From : \{Node\}$, $g.To : Node$. $g.Type = Async$.
\begin{equation}\label{Rule:ActivityForwardManyInputsOr}
    \frac{
        \begin{matrix}
            \exists \{ns\} \in g.From (ns.State = (Done \lor Closed)) \land \\ \land g.To.State = (Open \lor Closed) \land g.To.PreCondition
        \end{matrix}
    }{
        \begin{matrix}
            \forall n \in ns (s.State := Closed) ;\; \\ g.To.State \neq Open \vdash (g.To.State := InProgress ;\; g.To.PreOperation)
        \end{matrix}
    }
\end{equation}
