\chapter{Изоморфизм Карри-Говарда и вывод из гипотез}

\section{Логический вывод с гипотезами}

В логических исчислениях применяется множество разнообразных методов доказательств, одним из которых является использование \textit{гипотез} (иными словами, выражений за знаком выводимости $\vdash$). Такая техника предполагает наличие набора правил, описывающих работу со знаком выводимости. Рассмотрим эти правила: теорему дедукции, следствия из неё и дополнительные способы применения \textit{modus ponens}.

\begin{theorem}
    (Теорема дедукции.) Если $A_1,...,A_{n-1},A_n \vdash B$, то $A_1,...,A_{n-1} \vdash A_n \supset B$.
\end{theorem}

\begin{remark}
    Теорема дедукции доказуема в позитивной импликативной логике Лукасевича, так как для доказательства требует наличия в качестве теорем логических формул $TS$ и $TK$ и правила вывода \textit{modus ponens}.
\end{remark}

\begin{consequence}
    Если $A \vdash B$, то $\vdash A \supset B$.
\end{consequence}

\begin{consequence}
    Если $A_1,...,A_n \vdash B$, то $C_1,...,C_m, A_1,...,A_n \vdash B$.
\end{consequence}

\begin{consequence}
    Если $\vdash B$, то $C_1,...,C_m \vdash B$.
\end{consequence}

\begin{consequence}
    Безразлично, в каком порядке расположены гипотезы и сколько раз они повторяются.
\end{consequence}

\begin{remark}
    При оформлении доказательства будем использовать обозначение $DT(A)$, где $DT$ --- deduction theorem, а $A$ --- формула, по отношению к которой применяется теорема дедукции или одно из следствий теоремы дедукции.
\end{remark}

\begin{remark}
    Теорема дедукции позволяет передвигать символ выводимости в формуле <<влево>>. В то же время, для передвижения его <<вправо>> используется $\text{modus ponens}$. Действительно, по определению он записывается следующим образом: $A, A \supset B \vdash B$. Если же $\vdash A \supset B$, то эту гипотезу можно отбросить и сразу записать $A \vdash B$. Такую операцию над формулой при оформлении доказательства будем записывать как $MP(A)$.
\end{remark}

\begin{definition}
    Правило вывода \textit{modus ponens} с контекстом (наборами гипотез) записывается следующим образом:

    \begin{equation*}
        \frac{
            \Gamma \vdash A, \ \ \ \Delta \vdash A \supset B
        }{
            \Gamma, \Delta \vdash B
        }
    \end{equation*}
\end{definition}

\begin{remark}
    В силу следствия из теоремы дедукции, в целом не важно, в каком порядке записывать контекст $\Gamma$ и $\Delta$ при использовании предыдущего определения.
\end{remark}

\begin{remark}
    В качестве теоремы любого исчисления, использующего \textit{modus ponens}, будем использовать теорему $A, A \supset B \vdash B$ и обозначать её как $MP$.
\end{remark}

\section{Формулировка правил изоморфизма Карри-Говарда для данного типа вывода}

Изоморфизм Карри-Говарда в базовой формулировке описывает то, каким образом логические выражения соотносятся с типами объектов. В то же время, в рамках данной лабораторной ставится задача сопоставления логического вывода $\lambda$-термам на каждом шаге вывода. В связи с этим представляет интерес следующий вопрос --- каким образом должно происходить преобразование $\lambda$-терма при использовании в логическом выводе гипотез?

Сформулируем базовые правила изоморфизма Карри-Говарда:

\begin{equation*}
    \begin{array}{|c|c|}
        \hline
        \text{Правило вывода} & \text{Терм} \\ \hline
        A & A \\ \hline
        MP(B, A) & AB \\ \hline
    \end{array}
\end{equation*}

Рассмотрим пример вывода логического выражения $\vdash b \supset c \supset (a \supset b \supset (a \supset c))$ (теоремы $B$). Известно, что данному логическому выражению соответствует комбинатор $B=S(KS)K$, убедимся в этом, используя базовые правила изоморфизма Карри-Говарда:

\begin{equation*}
    \begin{array}{|l|l|c|r|}
        \hline 
        \text{№} & \text{Формула} & \text{Правило} & \text{Терм}
        \\ \hline
        1.  & \vdash s \supset (p \supset q) \supset (s \supset p \supset (s \supset q)) 
            & S 
            & S
            \\ \hline
        2.  & \vdash q \supset r \supset (p \supset (q \supset r) \supset (p \supset q \supset (p \supset r))) \supset & & \\ 
            & \ \ \ (q \supset r \supset (p \supset (q \supset r)) \supset & & \\ 
            & \ \ \ (q \supset r \supset (p \supset q \supset (p \supset r)))) 
            & [q \supset r/s, & \\ 
            & & p \supset q \supset (p \supset r)/q, & \\ 
            & & p \supset (q \supset r)/p](1) 
            & S 
            \\ \hline
        3.  & \vdash p \supset (q \supset p) 
            & K 
            & K 
            \\ \hline
        4.  & \vdash s \supset (p \supset q) \supset (s \supset p \supset (s \supset q)) \supset & & \\ 
            & \ \ \ (r \supset (s \supset (p \supset q) \supset (s \supset p \supset (s \supset q)))) 
            & [1/p, r/q](3) 
            & K
            \\ \hline
        5.  & \vdash r \supset (s \supset (p \supset q) \supset (s \supset p \supset (s \supset q))) 
            & MP(1, 4) 
            & KS
            \\ \hline
        6.  & \vdash q \supset r \supset (p \supset (q \supset r) \supset (p \supset q \supset (p \supset r))) 
            & [q \supset r/r, p/s, q/p, r/q](5) 
            & KS
            \\ \hline
        7.  & \vdash q \supset r \supset (p \supset (q \supset r)) \supset & & \\ 
            & \ \ \ (q \supset r \supset (p \supset q \supset (p \supset r))) 
            & MP(6, 2) 
            & S(KS)
            \\ \hline
        8.  & \vdash q \supset r \supset (p \supset (q \supset r)) 
            & [q \supset r/p, p/q](3) 
            & K
            \\ \hline
        9.  & \vdash q \supset r \supset (p \supset q \supset (p \supset r)) 
            & MP(8, 7)
            & S(KS)K
            \\ \hline
    \end{array}
\end{equation*}

Соответствующий $\lambda$-терм: $S(KS)K$.

Альтернативным доказательством с использованием гипотез является следующее:

\begin{equation*}
    \begin{array}{llcr}
        1.  & a \supset b, a \vdash b
            & MP
            & %\lambda x y. xy
            \\
        2.  & b, b \supset c \vdash c
            & MP
            & %\lambda u w. w u
            \\
        3.  & \vdash b \supset (b\supset c \supset c)
            & TD(2)
            & %\lambda u w. w u
            \\
        4.  & a \supset b, a \vdash b\supset c \supset c
            & MP(1,3)
            & %\lambda x y. (\lambda u w.w u) (x y)
            \\
        5.  & a \supset b, a, b\supset c \vdash c
            & MP(4)
            & %\lambda x y w. w(xy)
            \\
        6.  & a\supset b, b \supset c, a \vdash c
            & TD(5)
            & %\lambda x w y.w(xy)
            \\
        7.  & b \supset c, a\supset b, a \vdash c
            & TD(6)
            & %\lambda w x y.w(xy)
            \\
        8.  & \vdash b\supset c \supset (a \supset b \supset (a \supset c))
            & TD(7)
            & %\lambda w x y.w(xy)
            \\
    \end{array}
\end{equation*}

Как видно, в данном выводе не использовались аксиомы $K$ и $S$, однако в соответствии с изоморфизмом Карри-Говарда каждому шагу данного вывода соответствует некоторый $\lambda$-терм. 

Сформулируем дополнительные правила, которые, в отличие от сформулированных ранее, будут включать в себя также запись формул:

\begin{equation*}
    \begin{array}{|c|c|c|c|c|c|}
        \hline
        \text{№} & \text{Терм} & \text{Формула} & \text{Правило вывода} & \text{Формула} & \text{Терм} 
        \\ \hline
        1. & F & \vdash A \supset B & MP(F) & A \vdash B & \lambda x.Fx 
        \\ \hline
        2. & F & \vdash A \supset B & MP(G,F) & C \vdash B & \lambda x.(F)(Gx) 
        \\ 
        & \lambda x.Gx & C \vdash A & & & 
        \\ \hline
        3. & \lambda x.Fx & C \vdash A \supset B & MP(G,F) & C \vdash B & \lambda x.(Fx)(G)
        \\ 
        & G & \vdash A & & & 
        \\ \hline
        4. & \lambda xy.Fxy & C, B \vdash A & TD(F) & B, C \vdash A & \lambda yx.Fxy
        \\ \hline
        5. & \lambda x.Fx & B \vdash A & TD(F) & \vdash B \supset A & F 
        \\ \hline
        6. & F & \vdash A & TD(A) & B \vdash A & \lambda x. F
        \\ \hline
    \end{array}
\end{equation*}

Прокомментируем каждое из правил.

\begin{enumerate}
    \item Правило говорит о том, что перенос части формулы за знак выводимости не изменяет терм с точки зрения его типа (так как имеется правило $\eta$), однако даёт определения гипотезы с точки зрения $\lambda$-терма. Гипотезой в данном случае является $x$ --- переменная, по которой происходит абстрагирование. Этой переменной $x$ соответствует формула $A$, находящаяся за знаком выводимости.
    \item Данное правило описывает, каким образом видоизменяется терм при использовании \textit{modus ponens} с контекстом. В левом столбце терм $G$ сразу представляется с выделенной переменой $x$, которой соответствует формула $C$, стоящая за знаком выводимости. При осуществлении $MP(G,F)$ аппликация строится для термов, находящихся <<внутри>> контекста: для $F$ и $Gx$. Эта особенность показана явной расстановкой скобок в аппликации.
    \item Аналогичное предыдущему правило описывает случай, когда гипотеза имеется у большей посылки \textit{modus ponens}.
    \item Данное правило описывает следствие теоремы дедукции о том, что гипотезы можно произвольно менять местами. В данном случае рассматривается перестановка местами двух гипотез. Соответственно изменяется порядок следования переменных под $\lambda$-абстракцией в терме: с $\lambda xy$ на $\lambda yx$.
    \item Данное правило описывает непосредственно формулировку теоремы дедукции. При её использовании гипотезы исчезают, а значит \textbf{можно} убрать абстракцию по $x$ в соответствии с правилом редукции $\eta$. В то же время, её можно не убирать и оставить терм в прежнем виде, если это будет удобно для дальнейшего вывода.
    \item Данное правило описывает следствие теоремы дедукции, описывающее возможность добавления за знак вывода произвольной посылки.
\end{enumerate}

\begin{remark}
    Несложно заметить, что теореме $MP$ ($A \supset B, A \vdash B$) будет соответствовать терм $\lambda xy.xy$.
\end{remark}

Теперь с использованием сформулированный правил имеется возможность составить $\lambda$-терм для альтернативного доказательства теоремы $B$.

\begin{equation*}
    \begin{array}{llcr}
        1.  & a \supset b, a \vdash b
            & MP
            & \lambda x y. xy
            \\
        2.  & b, b \supset c \vdash c
            & MP
            & \lambda u w. w u
            \\
        3.  & \vdash b \supset (b\supset c \supset c)
            & TD(2)
            & \lambda u w. w u
            \\
        4.  & a \supset b, a \vdash b\supset c \supset c
            & MP(1,3)
            & \lambda x y. (\lambda u w.w u) (x y)
            \\
        5.  & a \supset b, a, b\supset c \vdash c
            & MP(4)
            & \lambda x y w. w(xy)
            \\
        6.  & a\supset b, b \supset c, a \vdash c
            & TD(5)
            & \lambda x w y.w(xy)
            \\
        7.  & b \supset c, a\supset b, a \vdash c
            & TD(6)
            & \lambda w x y.w(xy)
            \\
        8.  & \vdash b\supset c \supset (a \supset b \supset (a \supset c))
            & TD(7)
            & \lambda w x y.w(xy)
            \\
    \end{array}
\end{equation*}

Получили терм $\lambda w x y.w(xy)$. Действительно, по его структуре видно, что он описывает комбинаторную характеристику комбинатора $B$: $Bwxy=w(xy)$.

\begin{lstlisting}[
    label=Listing:Theory,
    caption={Вывод типа полученного терма при помощи OCaml},
    language=Caml
]
# fun w x y -> w (x y) ;;
- : ('a -> 'b) -> ('c -> 'a) -> 'c -> 'b = <fun>
\end{lstlisting}

С точностью до имён переменный получили, что составлен корректный терм: его тип совпадает с доказанным логическим выражением.

\section{Выводы}

Сформулированы теорема дедукции, её следствия и правило вывода \textit{modus ponens} с контекстом. Получены правила, описывающие применение изоморфизма Карри-Говарда к логическим выводам, содержащим гипотезы. Показано, каким образом возможно конструировать $\lambda$-термы по формулам, содержащим гипотезы, а также представлен пример доказательства теоремы и соответствующего ему построения $\lambda$-терма.