(* Task 6 *)

type json1 = string * (int * int * int) list

type json2 = string * int list list

type json3 = string * string list
