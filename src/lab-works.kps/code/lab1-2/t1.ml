(* Task 1 *)
(* 1 *)
5 + 5;;

(* 2 *)
12.5 -. 8.11;;

(* 3 *)

2.0 ** 3.0;;

(* 4 *)

"";;
"hello world!";;

(* 5 *)

"hello" ^ " " ^ "world";;

(* 6 *)

true && false || true && true || false && (not true);;

;;