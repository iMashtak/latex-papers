(* Task 2 *)

let rec get index items =
  match items with
  | [] -> raise (Failure "index out of range")
  | head :: tail -> if index == 0 then head else get (index - 1) tail
;;

get 2 [ 0; 1; 2; 3; 4 ];;

get 2 [];;

get 0 [];;

get 2 [ 0; 1 ];;
