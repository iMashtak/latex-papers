(* Task 4 *)

let rec map func items =
  match items with [] -> [] | head :: tail -> func head :: map func tail
;;

map (fun x -> x + 1) [ 1; 2; 3; 4 ];;

let div x = [ x - 2; x + 2 ] in
map div [ 1; 2; 3; 4 ]
