(* Task 3 *)

let rec invert items =
  match items with [] -> [] | head :: tail -> invert tail @ [ head ]
;;

invert [ 1; 2; 3; 4; 5; 6 ];;

invert [ 1; 2; 3; 4; 5; 6; 7 ];;

invert [];;
