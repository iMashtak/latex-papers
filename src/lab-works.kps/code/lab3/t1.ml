(* Task 1 *)

let rec length x = match x with [] -> 0 | head :: tail -> 1 + length tail;;

length [];;

length [ 1 ];;

length [ 3; 4; 5 ];;

length [ []; [] ];;

length [ [ 2; 4 ]; [ 5; 6; 7; 3 ]; []; [ 2; 3; 4; 5 ] ];;