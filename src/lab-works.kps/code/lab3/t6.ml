(* Task 6 *)

let rec length x = match x with [] -> 0 | head :: tail -> 1 + length tail
;;

let rec reduce func acc items =
  match items with
  | [] -> acc
  | head :: tail -> reduce func (func acc head) tail
;;

let min items =
  let rec min' m xs =
    match xs with
    | [] -> m
    | head :: tail -> if head < m then min' head tail else min' m tail
  in
  min' (List.hd items) items
;;

min [ 5; 3; 8; 2; 4 ];;

let max items =
  let rec max' m xs =
    match xs with
    | [] -> m
    | head :: tail -> if head > m then max' head tail else max' m tail
  in
  max' (List.hd items) items
;;

min [ 5; 3; 8; 2; 4 ];;

let sum items = reduce (fun x y -> x + y) 0 items;;

sum [ 1; 2; 3; 4 ];;

let avg items = sum items / length items;;

avg [ 1; 2; 3; 4 ];;
