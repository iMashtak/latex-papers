(* Task 5 *)

let rec reduce func acc items =
  match items with
  | [] -> acc
  | head :: tail -> reduce func (func acc head) tail
;;

reduce (fun x y -> x + y) 1 [ 3; 4; 5 ];;

reduce (fun x y -> x + y) 0 [ 3; 4; 5 ];;

