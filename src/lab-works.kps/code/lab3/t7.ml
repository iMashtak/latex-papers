(* Task 7 *)

let rec map func items =
  match items with [] -> [] | head :: tail -> func head :: map func tail

let rec reduce func acc items =
  match items with
  | [] -> acc
  | head :: tail -> reduce func (func acc head) tail

let flatmap func items = reduce (fun x y -> x @ y) [] (map (map func) items);;

flatmap Fun.id [ [ 1; 2 ]; [ 3; 4 ]; [ 5; 6 ]; [ 7; 8 ] ];;
