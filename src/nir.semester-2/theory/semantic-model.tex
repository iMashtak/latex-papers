\subsection{Концептуальная модель процесса}

\begin{figure}[ht]
    \begin{center}
        \includegraphics[width=.9\linewidth]{\pwd/img/SemanticProcessModel.png}
    \end{center}
    \caption{Концептуальная модель процессов, представленная в виде ER-диаграммы}
    \label{Picture:SemanticProcessModel}
\end{figure}

Кратко напомним сформулированную в рамках предыдущих УИР/НИР семантическую модель процессов. На рисунке \ref{Picture:SemanticProcessModel} представлена концептуальная модель процесса в виде Entity relationship диаграммы.

Исходя из диаграммы можно заключить, что процесс (Process) обладает внутренней структурой, которая определяется стадиями и связями (Stage, Gate), вместе называемых узлами (Node) процесса, а также внешним окружением, к которому относятся артефакты, роли и события (Artifact, Role, Event).

\subsection{Состояния, типы и виды связей}

Выделяется 4 состояния узла, 3 типа связей и 3 способа соединения узлов. Опишем эти возможности:

\begin{compactitem}
    \item Состояния узлов: 
    \begin{compactenum}
        \item \textit{Open} --- начальное состояние каждого узла.
        \item \textit{InProgress} --- состояние, обозначающее, что данный узел сейчас активен и для продолжения работы процесса необходимо его перевести в состояние \textit{Done} (завершился).
        \item \textit{Done} --- состояние, обозначающее, что данный узел был активным, но только что завершился.
        \item \textit{Closed} --- конечное состояние узла.
    \end{compactenum}
    Состояние любого узла последовательно переходит от \textit{Open} к \textit{Closed}. В рамках семантической модели процесса возможно возникновение циклов; в таком случае состояние \textit{Closed} сменится на состояние \textit{InProgress}.

    \item Способы соединения узлов:
    \begin{compactenum}
        \item $1 \to 1$ --- простая последовательная связь двух узлов между собой. Тип связи не требуется.
        \item $1 \to N$ --- связь вида "один ко многим". Требует для себя указания типа связи.
        \item $N \to 1$ --- связь вида "многие к одному". Требует для себя указания типа связи.
    \end{compactenum}
    Соединение вида $N \to M$ не вводится в качестве базисного, так как оно реализуется в виде последовательного использования соединений $N \to 1$ и $1 \to M$. В этом случае потребуется промежуточная связь. 

    \item Типы связей:
    \begin{compactenum}
        \item $\text{AND}(1 \to N)$ --- запускает $N$ узлов одновременно и параллельно, дожидаясь возможности запуска каждого из этих узлов.
        \item $\text{AND}(N \to 1)$ --- ожидает завершения $N$ узлов и только после этого запускает исходящий узел.
        \item $\text{XOR}(1 \to N)$ --- из имеющихся на выходе $N$ узлов выбирает первый, который возможно завершить, и запускает только его.
        \item $\text{XOR}(N \to 1)$ --- из имеющихся на входе $N$ узлов ищет первый завершённый, после чего завершает только его и запускает исходящий узел.
        \item $\text{OR}(1 \to N)$ --- из имеющихся на выходе $N$ узлов выбираются те, которые возможно запустить и они запускаются. Так может быть запущено произвольное количество узлов, но хотя бы один.
        \item $\text{OR}(N \to 1)$ --- из имеющихся на входе $N$ узлов выбираются те, которые можно завершить, и они завершаются. Так может быть завершено произвольное количество узлов, но хотя бы один.
        \item $\text{ASYNC}(1 \to N)$ --- по мере того, как становится возможным запустить некоторый из $N$ узлов, он запускается.
        \item $\text{ASYNC}(N \to 1)$ --- по мере того, как становится возможным завершить некоторый из $N$ узлов, он завершается.
    \end{compactenum}
    Принципиальным отличием типов связей $\text{AND}$, $\text{XOR}$, $\text{OR}$ от типа связи $\text{ASYNC}$ является то, что связь, имеющая тип $\text{ASYNC}$, не может быть переведена в состояние завершённой. Такие связи позволяют организовывать событийно-ориентированное исполнение процесса в случае, если это необходимо. Тип связи $\text{ASYNC}$ позволяет реализовывать в рамках модели процессов такую концепцию, как шина событий (шина сообщений) \cite{Study:Words-About-Event-Bus}.
\end{compactitem}

\subsection{Правила исполнения процесса}

Также для описания проистечения процесса вводится набор правил перехода --- пар вида (условие, процедура). 

\begin{compactitem}
    \item Правила записываются в виде двух выражений, разделённых горизонтальной чертой. Сверху записывается набор условий, а снизу - набор действий. При выполнении всех условий выполняются все действия.
    \item Множество элементов типа $T$ обозначается как $\{T\}$.
    \item Знак $=$ обозначает оператор сравнения на эквивалентность.
    \item Знак $:=$ обозначает операцию присвоения.
    \item Конструкция $object.Attribute$ обозначает доступ к значению атрибута $Atribute$ объекта $object$.
    \item Конструкция $\langle event \rangle$ обозначает требования свершения события $event$ в рамках исполнения процесса. Если используется в условии правила, то проверяется свершение события. Если используется в действии, то событие совершается.
    \item Конструкция $o:Object$ описывает отношение типизации, объект $o$ имеет тип $Object$.
    \item Конструкция $\exists \{os\} (condition)$ означает требование существования некоторого подмножества, для каждого элемента $os$ которого выполняется условие $condition$.
\end{compactitem}

Также для краткости примем некоторые умолчания об именах объектов. Пусть имена объектов начинаются с той же буквы, что начинается их тип, но только не с заглавной, а с прописной. То есть $p:Process$, $n:Node$, $g:Gate$, $s:Stage$, $e:Event$.

\textbf{Вырожденное состояние узла-связи $g$.} Хотя для любого узла устанавливается 4 возможных состояния, состояние \textit{InProgress} для связи не имеет смысла (ведь не существует понятия <<выполнение связи>>). В связи с этим это состояние сразу переводится в состояние \textit{Done}.
\begin{equation}\label{Rule:GateUselessState}
    \frac{
        g.State = InProgress
    }{
        g.State := Done
    }
\end{equation}

\textbf{Передача активности для связи $g$ с типом соединения $1 \to 1$.} Формально этот случай описывается следующим условием: $g.From : Node$, $g.To : Node$;
\begin{equation}\label{Rule:ActivityForwardSimpleGate}
    \frac{
        g.From.State = Done \land g.To.State = (Open \lor Closed) \land g.To.PreCondition
    }{
        g.From.State := Closed ;\; g.To.State := InProgress ;\; g.To.PreOperation
    }
\end{equation}

\textbf{Передача активности для связи $g$ типа AND с типом соединения $1 \to N$.} Формально этот случай описывается следующим условием: $g.From : Node$, $g.To : \{Node\}$, $g.Type = And$.
\begin{equation}\label{Rule:ActivityForwardManyOutputsAnd}
    \frac{
        g.From.State = Done \land \forall n \in g.To (n.State = (Open \lor Closed) \land n.PreCondition)
    }{
        g.From.State := Closed ;\; \forall n \in g.To (n.State := InProgress ;\; n.PreOperation)
    }
\end{equation}

\textbf{Передача активности для связи $g$ типа AND с типом соединения $N \to 1$.} Формально этот случай описывается следующим условием: $g.From : \{Node\}$, $g.To : Node$. $g.Type = And$.
\begin{equation}\label{Rule:ActivityForwardManyInputsAnd}
    \frac{
        \forall n \in g.From (n.State = Done) \land g.To.State = (Open \lor Closed) \land g.To.PreCondition
    }{
        \forall n \in g.From (n.State := Closed) ;\; g.To.State := InProgress ;\; g.To.PreOperation
    }
\end{equation}

\textbf{Передача активности для связи $g$ типа XOR с типом соединения $1 \to N$.} Формально этот случай описывается следующим условием: $g.From : Node$, $g.To : \{Node\}$, $g.Type = Xor$.
\begin{equation}\label{Rule:ActivityForwardManyOutputsXor}
    \frac{
        g.From.State = Done \land \exists! n \in g.To (n.State = (Open \lor Closed) \land n.PreCondition)
    }{
        g.From.State := Closed ;\; n.State := InProgress ;\; n.PreOperation
    }
\end{equation}

\textbf{Передача активности для связи $g$ типа XOR с типом соединения $N \to 1$.} Формально этот случай описывается следующим условием: $g.From : \{Node\}$, $g.To : Node$. $g.Type = Xor$.
\begin{equation}\label{Rule:ActivityForwardManyInputsXor}
    \frac{
        \exists! n \in g.From (n.State = Done) \land g.To.State = (Open \lor Closed) \land g.To.PreCondition
    }{
        n.State := Closed ;\; g.To.State := InProgress ;\; g.To.PreOperation
    }
\end{equation}

\textbf{Передача активности для связи $g$ типа OR с типом соединения $1 \to N$.} Формально этот случай описывается следующим условием: $g.From : Node$, $g.To : \{Node\}$, $g.Type = Or$.
\begin{equation}\label{Rule:ActivityForwardManyOutputsOr}
    \frac{
        g.From.State = Done \land \exists \{ns\} \in g.To (ns.State = (Open \lor Closed) \land ns.PreCondition)
    }{
        g.From.State := Closed ;\; \forall n \in \{ns\} (n.State := InProgress ;\; n.PreCondition)
    }
\end{equation}

\textbf{Передача активности для связи $g$ типа OR с типом соединения $N \to 1$.} Формально этот случай описывается следующим условием: $g.From : \{Node\}$, $g.To : Node$. $g.Type = Or$.
\begin{equation}\label{Rule:ActivityForwardManyInputsOr}
    \frac{
        \exists \{ns\} \in g.From (ns.State = Done) \land g.To.State = (Open \lor Closed) \land g.To.PreCondition
    }{
        \forall n \in ns (s.State := Closed) ;\; g.To.State := InProgress ;\; g.To.PreOperation
    }
\end{equation}

\textbf{Передача активности для связи $g$ типа ASYNC с типом соединения $1 \to N$.} Формально этот случай описывается следующим условием: $g.From : Node$, $g.To : \{Node\}$, $g.Type = Async$.
\begin{equation}\label{Rule:ActivityForwardManyOutputsAsync}
    \frac{
        \begin{matrix}
            g.From.State = (Done \lor Closed) \land \\ \land \exists \{ns\} \in g.To (ns.State = (Open \lor Closed) \land ns.PreCondition)
        \end{matrix}
    }{
        g.From.State = Closed ;\; \forall n \in ns (n.State := InProgress \land n.PreOperation)
    }
\end{equation}

\textbf{Передача активности для связи $g$ типа ASYNC с типом соединения $N \to 1$.} Формально этот случай описывается следующим условием: $g.From : \{Node\}$, $g.To : Node$. $g.Type = Async$.
\begin{equation}\label{Rule:ActivityForwardManyInputsOr}
    \frac{
        \begin{matrix}
            \exists \{ns\} \in g.From (ns.State = (Done \lor Closed)) \land \\ \land g.To.State = (Open \lor Closed) \land g.To.PreCondition
        \end{matrix}
    }{
        \begin{matrix}
            \forall n \in ns (s.State := Closed) ;\; \\ g.To.State \neq Open \vdash (g.To.State := InProgress ;\; g.To.PreOperation)
        \end{matrix}
    }
\end{equation}
