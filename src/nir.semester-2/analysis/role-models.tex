\section{Обзор методов моделирования ролевой структуры процесса}

\subsection{Дискреционная модель}

\textbf{Дискреционная модель} представляет ролевую структуру в виде набора прав, приписанных каждому объекту в системе отдельно при помощи списков управления доступом (Access Control List, ACL) \cite{Study:DAC}. Списки управления доступом также называют матрицами доступа, хотя реализация дискреционной модели не обязывает создавать и обслуживать полноценную матрицу, так как в реальных случаях такая матрица получалась бы сильно разреженной. Обычно используются списки, где каждому конкретному объекту сопоставляются права относительно участников ситуации. Иногда участников объединяют в группы, но это осуществляется лишь в целях экономии ресурсов.

Формально говоря, имеется множество субъектов $S$, множество объектов $O$ и множество прав доступа $P$. Тогда дискреционная модель может быть описана как множество кортежей $DAC=\{(s, o, [p_1,...,p_n]) | s\in S, o\in O, p_1 \dots p_n \in P\}$, где $s$ --- некоторый субъект, $o$ --- некоторый объект, $[p_1,...,p_n]$ --- список прав, имеющихся у субъекта $s$ по отношению к объекту $o$.

Существует несколько подходов к тому, каким образом в данной модели изменять список прав для каждого кортежа $d\in D$:

\begin{compactitem}
    \item Для каждого объекта $o\in O$ определяется некоторый владелец $s \in S$, который может редактировать список прав тех кортежей, в которых присутствует объект $o$.
    \item Субъекты, обладающие некими правами относительно объектов могут передавать их другим субъектам в системе.
    \item В системе выделяется некоторый субъект, которому разрешается на своё усмотрение устанавливать права во всех кортежах системы.
\end{compactitem}

Все три перечисленных подхода используются, например, в операционных системах при управлении правами доступа к файлам в файловой системе. 

Данный подход предоставляет возможность максимально точно настраивать права доступа к элементам системы, а значит и определять роль участника в системе. Однако модель не предполагает каких-либо средств для абстрагирования и выстраивания абстрактных правил управления доступом.

\subsection{Мандатная модель}

\textbf{Мандатная модель} предлагает разграничение доступа объектов к субъектам на основе специальных меток, которые присваиваются объектам \cite{Study:MAC}, при этом на множестве меток определяется отношение порядка (образуется иерархия меток). Чтобы субъект мог получить доступ к объекту, ему необходимо обладать разрешением на доступ к объектам, обладающим конкретной меткой. Причём субъект в таком случае получает доступ и ко всем объектам, чья метка расположена ниже (например, данный объект представляет собой менее конфиденциальную информацию) в иерархии.

Формально данную модель можно описать следующим образом. Пусть имеется множество субъектов $s_m \in S$ и множество объектов $o_n \in O$, причём $m,n \in M$ --- метки конфиденциальности. Пусть на множестве $M$ задано отношение частичного порядка $\le$. Тогда мандатная модель задаётся в виде предиката $MAC(s_m, o_n) = n \le m$. В случае, если значение предиката равно истинному, то субъекту $s$ разрешается доступ к объекту $o$. В ином случае доступ запрещается.

Таким образом, доступ к объектам декларируется самим объектом. Это позволяет использовать в системе политики безопасности, которые устанавливают права доступа к объекту в зависимости от его типа. Такой подход приводит к ключевой особенности мандатной модели --- субъект не может полностью управлять доступом к объектам, которые он создаёт. Ещё одно следствие мандатной модели заключается в том, что субъекты сами по себе не обладают правами, таким образом, они не могут передать часть своих прав другим субъектам, что возможно в дискреционной модели.

Недостатком данной модели может быть необходимость фиксирования количества и иерархии меток, так как добавление новых меток будет требовать пересмотра приписываний для всех имеющихся объектов в системе, что нередко бывает невозможным.

\subsection{Ролевая модель}

\textbf{Ролевая модель} является развитием дискреционной модели управления доступом, хотя достаточно сильно от неё отличается. В рамках данной модели права доступа к объектам группируются в роли, которые, в свою очередь, приписываются каждому субъекту системы. Таким образом каждому участнику системы приписывается не отдельное право само по себе, а фиксированное множество прав относительно группы объектов.

Формальное определение может быть следующим (опираясь на работу \cite{Study:Role-Based-Access-Control}). Пусть имеются следующие множества: $S$ субъектов, $O$ объектов, $P$ прав доступа, $R$ ролей. Пусть дополнительно на множестве $R$ определено отношение частичного порядка $\le$. Сама же ролевая модель представляется в виде $n$-ки вида $RBAC = (S, O, R, SR=S \times R, RP = R \times P)$, где $SR$ --- отношение "многие-ко-многим" между множеством субъектов и множеством ролей (то есть один субъект может иметь несколько ролей одновременно), а $RP$ --- отношение "многие-ко-многим" между множеством ролей и множеством прав доступа (то есть одно право доступа может относиться к нескольким ролям).

Ролевую модель удобно использовать, когда имеется фиксированное количество ролей и каждой из них сопоставляется расширяемое множество прав. Изменение состава ролей будет требовать пересмотра ролей всех участников системы, что зачастую невозможно в силу технических или организационных причин.

\subsection{Модель на основе атрибутов}

\textbf{Модель разграничения доступа на основе аттрибутов} --- модель контроля доступа к объектам, в которой запросы субъекта на исполнение операций над объектами допускаются или не допускаются в зависимости от аттрибутов самого пользователя в системе, аттрибутов объекта, условий, сложившихся в системе, а также набора политик, определяемых через перечисленные атрибуты и условия \cite{Study:Attribute-Based-Access-Control}. Аттрибуты являются характеристиками субъекта или объекта и содержат информацию в виде ключ-значение. Субъектом может быть пользователь системы или устройство, для которого определены операции. Объекты системы --- это сущности, доступом к которым необходимо управлять, такими сущностями могут быть записи в базе данных. Операции над объектами --- это произвольные функции, которые можно применить к объекту: чтение, запись, изменение, удаление и любые другие. Политики --- это наборы правил (предикатов, зависящих от аттрибутов системы и внешних условий), позволяющие определить, можно ли давать доступ субъекту, или нет. Под внешними условиями понимается контекст, в рамках которого исполняется запрос, причём природа этого контекста не уточняется в рамках модели.

Данная модель по сравнению с другими моделями предоставляет наиболее полно и корректно описывать правила доступа к конкретным объектам системы, но обладает схожими с дискреционной моделью проблемой -- размытостью понятия <<роль>>. Не составляет сложности установить роль как один из атрибутов пользователя и связать её с множеством объектов, относительно которых эта роль будет иметь смысл. В то же время, атрибутная модель может позволить осуществлять регулирование доступа с помощью дополнительных правил, не связанных с ролями. Поэтому на практике имеет смысл ограничивать возможности атрибутной модели для повышения прозрачности работы в системе.