#include <vector>
#include <set>
#include <iostream>
#include <stdexcept>

class Graph {
private:
    std::vector<int> starts;
    std::vector<int> ends;
    std::set<int> vertices;

    void traverse(std::set<int> &visited, int nodeId) const {
        for (int i = 0; i < starts.size(); i++) {
            if (starts[i] == nodeId) {
                if (visited.count(ends[i]) == 0) {
                    visited.insert(ends[i]);
                    traverse(visited, ends[i]);
                }
            }
        }
    }

public:
    Graph(
            const std::vector<int> &starts,
            const std::vector<int> &ends
    ) {
        if (starts.size() != ends.size()) {
            throw std::invalid_argument(
                    "unequal vectors of starts and ends"
            );
        }
        this->starts = starts;
        this->ends = ends;
        for (auto e : starts) {
            this->vertices.insert(e);
        }
        for (auto e : ends) {
            this->vertices.insert(e);
        }
    }

    int numOutgoing(const int nodeId) const {
        if (vertices.count(nodeId) == 0) {
            throw std::invalid_argument(
                    "node with id=" +
                    std::to_string(nodeId) +
                    " not found in graph"
            );
        }
        std::set<int> visited;
        traverse(visited, nodeId);
        return visited.size();
    }

    std::vector<int> adjacent(const int nodeId) const {
        if (vertices.count(nodeId) == 0) {
            throw std::invalid_argument(
                    "node with id=" +
                    std::to_string(nodeId) +
                    " not found in graph"
            );
        }
        std::set<int> visited;
        traverse(visited, nodeId);
        std::vector<int> result(visited.begin(), visited.end());
        return result;
    }
};

int main() {
    std::vector<int> starts{1, 2, 2, 3, 4, 5, 5};
    std::vector<int> ends{2, 3, 5, 4, 3, 3, 4};
    Graph graph(starts, ends);

    int nodeId = 1;
    std::cout << graph.numOutgoing(nodeId) << std::endl;
    auto adjacent = graph.adjacent(nodeId);
    for (auto vertex : adjacent) {
        std::cout << vertex << " ";
    }
}