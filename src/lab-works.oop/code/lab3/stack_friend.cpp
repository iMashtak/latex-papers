#include <stdexcept>
#include <vector>
#include <memory>
#include <iostream>

template<class T>
class Stack;

template<class T>
Stack<T> operator+(const Stack<T> &left, const Stack<T> &right);

template<class T>
class Stack {
private:
    int length;
    int size;
    T *data;

    Stack(int size, int length, T *data) {
        this->size = size;
        this->length = length;
        this->data = data;
    }

public:
    explicit Stack(int n) {
        size = n;
        length = 0;
        data = new T[n];
    }

    Stack(const Stack<T> &stack) {
        size = stack.size;
        length = stack.length;
        data = new T[size];
        memcpy(data, stack.data, sizeof(T) * length);
    }

    ~Stack() {
        delete[] data;
    }

    bool empty() {
        return length == 0;
    }

    T &top() {
        return data[length - 1];
    }

    void *push(T &item) {
        if (length == size) {
            auto old = data;
            size = size * 2;
            data = new int[size];
            for (auto i = 0; i < length; ++i) {
                data[i] = old[i];
            }
            delete[] old;
        }
        data[length++] = item;
        return this;
    }

    void pop() {
        if (length == 0) {
            throw std::invalid_argument("empty stack");
        }
        --length;
    }

    void print() const {
        std::cout << "Size: " << size << std::endl;
        std::cout << "Length: " << length << std::endl;
        std::cout << "Data: " << "[ ";
        for (int i = length - 1; i > -1; --i) {
            std::cout << data[i] << " ";
        }
        std::cout << "]" << std::endl;
    }

    friend Stack operator+<T>(
            const Stack &left,
            const Stack &right
    );
};

template<class T>
Stack<T> operator+(const Stack<T> &left, const Stack<T> &right) {
    auto size = left.size + right.size;
    auto length = left.length + right.length;
    auto data = new T[size];
    for (int i = 0; i < left.length; ++i) {
        data[i] = left.data[i];
    }
    for (int i = 0; i < right.length; ++i) {
        data[i + left.length] = right.data[i];
    }
    Stack<T> result(size, length, data);
    return result;
}

int main() {
    Stack<int> s1(5);
    Stack<int> s2(5);

    std::vector<int> f3{92, 101, 106, 108, 112};
    std::vector<int> fnv{3, 11, 22, 19, 34};

    for (auto v : f3) {
        s1.push(v);
    }
    for (auto v : fnv) {
        s2.push(v);
    }

    std::cout << "Stack f3" << std::endl;
    s1.print();
    std::cout << std::endl;
    std::cout << "Stack fnv" << std::endl;
    s2.print();
    std::cout << std::endl;

    auto s3 = s1 + s2;

    std::cout << "Stack f3+fnv" << std::endl;
    s3.print();
    std::cout << std::endl;

    return 0;
}