#include <stdexcept>
#include <vector>

template<class T>
class Stack {
private:
    int length;
    int size;
    T *data;
public:
    Stack(int n) {
        size = n;
        length = 0;
        data = new T[n];
    }

    ~Stack() {
        delete[] data;
    }

    bool empty() {
        return length == 0;
    }

    T &top() {
        return data[length - 1];
    }

    void *push(T &item) {
        if (length == size) {
            auto old = data;
            size = size * 2;
            data = new int[size];
            for (auto i = 0; i < length; ++i) {
                data[i] = old[i];
            }
            delete[] old;
        }
        data[length++] = item;
        return this;
    }

    void pop() {
        if (length == 0) {
            throw std::invalid_argument("empty stack");
        }
        --length;
    }
};

int main() {
    Stack<int> stack(10);

    std::vector<int> vaults {
        92, 101, 106, 108, 112,
        3, 11, 22, 19, 34
    };

    for (auto v : vaults) {
        stack.push(v);
    }
    while (!stack.empty()) {
        stack.top();
        stack.pop();
    }
    return 0;
}