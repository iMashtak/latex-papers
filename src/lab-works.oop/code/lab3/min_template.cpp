template<class T>
T min(T left, T right) {
    return left < right ? left : right;
}