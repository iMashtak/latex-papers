#include <iostream>
using namespace std;
class Expr {
public:
    virtual int eval() = 0;
};
class Num : public Expr {
private:
    int n;
public:
    Num(int n) {
        this->n = n;
    }
    int eval() override {
        return this->n;
    }
};
class Add : public Expr {
private:
    Expr *left;
    Expr *right;
public:
    Add(Expr *left, Expr *right) {
        this->left = left;
        this->right = right;
    }
    int eval() override {
        return this->left->eval() + this->right->eval();
    }
};
class Subtract : public Expr {
private:
    Expr *left;
    Expr *right;
public:
    Subtract(Expr *left, Expr *right) {
        this->left = left;
        this->right = right;
    }
    int eval() override {
        return this->left->eval() - this->right->eval();
    }
};
int main() {
    // (6 + 9) - ((4 - 5) + 3)
    auto expr = new Subtract(
            new Add(new Num(6), new Num(9)),
            new Add(
                    new Subtract(new Num(4), new Num(5)),
                    new Num(3)
            )
    );
    cout << expr->eval() << endl;
}
