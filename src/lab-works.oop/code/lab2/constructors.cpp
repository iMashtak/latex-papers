#include <iostream>
using namespace std;
class Parent {
public:
    Parent() { cout << "Parent.ctor" << endl; }
    ~Parent() { cout << "Parent.dtor" << endl; }
};
class ChildA : public Parent {
public:
    ChildA() { cout << "ChildA.ctor" << endl; }
    ~ChildA() { cout << "ChildA.dtor" << endl; }
};
class ChildB : public Parent {
public:
    ChildB() { cout << "ChildB.ctor" << endl; }
    ~ChildB() { cout << "ChildB.dtor" << endl; }
};
class Childish: public ChildA, public ChildB {
public:
    Childish() { cout << "Childish.ctor" << endl; }
    ~Childish() { cout << "Childish.dtor" << endl; }
};
int main() {
    auto o = new Childish();
    delete o;
}
