#include <stdexcept>
#include <iostream>
#include <vector>

using namespace std;

class Collection {
protected:
    int length;
    int size;
    int *data;
public:
    Collection(int n) {
        size = n;
        length = 0;
        data = new int[n];
    }

    void print() {
        cout << "[ ";
        for (auto i = 0; i < length; ++i) {
            cout << get(i) << " ";
        }
        cout << "]";
    }

    virtual int get(int i) = 0;

    virtual Collection *put(int element) = 0;

    virtual int take() = 0;

    ~Collection() {
        delete[] data;
    }
};

class Stack : public Collection {
public:
    Stack(int n) : Collection(n) {}

    int get(int i) override {
        return data[length - 1 - i];
    }

    Collection *put(int element) override {
        if (length == size) {
            auto old = data;
            size = size * 2;
            data = new int[size];
            for (auto i = 0; i < length; ++i) {
                data[i] = old[i];
            }
            delete[] old;
        }
        data[length++] = element;
        return this;
    }

    int take() override {
        if (length == 0) {
            throw std::invalid_argument("empty stack");
        }
        return data[--length];
    }
};

class Queue : public Collection {
private:
    int offset;
public:
    Queue(int n) : Collection(n) {
        offset = 0;
    }

    int get(int i) override {
        return data[offset + i];
    }

    Collection *put(int element) override {
        if (offset + length == size) {
            auto old = data;
            if (offset <= size / 3) { size = size * 2; }
            data = new int[size];
            for (auto i = 0; i < length; ++i) {
                data[i] = old[i];
            }
            offset = 0;
            delete[] old;
        }
        data[length++] = element;
        return this;
    }

    int take() override {
        if (length == 0) {
            throw std::invalid_argument("empty queue");
        }
        --length;
        return data[offset++];
    }
};

int main() {
    vector<Collection *> collections = {
            (new Stack(10))->put(1)->put(2)->put(3),
            (new Queue(10))->put(1)->put(2)->put(3),
            (new Stack(2))->put(4)->put(11)->put(12),
            (new Queue(2))->put(5)->put(10)->put(13),
            (new Stack(3))->put(6)->put(9)->put(14),
            (new Queue(3))->put(7)->put(8)->put(15)
    };
    for (auto collection : collections) {
        collection->put(100);
        collection->take();
    }
    for (auto collection : collections) {
        collection->print();
        cout << endl;
        delete collection;
    }
}