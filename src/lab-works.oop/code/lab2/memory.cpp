#include <iostream>
#include <cstring>
using namespace std;
class Collection {
protected:
    int length;
    int size;
    int *data;
public:
    Collection(int n) {
        size = n;
        length = 0;
        data = new int[n];
    }
    Collection(const Collection& c) {
        data = new int[c.size];
        memcpy(data, c.data, c.size*sizeof(int));
        length = c.length;
        size = c.size;
    }
    virtual int get(int i) = 0;
    virtual Collection *put(int element) = 0;
    virtual int take() = 0;
    ~Collection() {
        delete[] data;
    }
};

class Queue : public Collection {
private:
    int offset;
public:
    Queue(int n) : Collection(n) {
        offset = 0;
    }
    int get(int i) override {
        return data[offset + i];
    }
    Collection *put(int element) override {
        if (offset + length == size) {
            auto old = data;
            if (offset <= size / 3) { size = size * 2; }
            data = new int[size];
            for (auto i = 0; i < length; ++i) {
                data[i] = old[i];
            }
            offset = 0;
            delete[] old;
        }
        data[length++] = element;
        return this;
    }
    int take() override {
        if (is_empty()) {
            throw std::invalid_argument("empty queue");
        }
        --length;
        return data[offset++];
    }
    bool is_empty() {
        return length == 0;
    }
};
void op_1(Queue q) {
    while(!q.is_empty()) {
        cout << q.take() << " ";
    }
}
void op_2(Queue &q) {
    while(!q.is_empty()) {
        cout << q.take() << " ";
    }
}

void op_3(Queue *q) {
    while(!q->is_empty()) {
        cout << q->take() << " ";
    }
}
int main() {
    Queue q(10000);
    q.put(1)->put(2)->put(3)->put(4)->put(5);
//    op_1(q);
//    op_2(q);
//    op_3(&q);
    return 0;
}
