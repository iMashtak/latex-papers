namespace common {
    int plus(int left, int right) {
        return left + right;
    }
}
namespace alternative {
    int plus(int left, int right) {
        return (left + right) % 2;
    }
}
#include <iostream>
int main() {
    std::cout << common::plus(10,20) << std::endl; // 30
    std::cout << alternative::plus(10, 20) << std::endl; // 0
}