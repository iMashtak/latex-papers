#include <iostream>
void proc_1(int item) { item += 100; }
void proc_2(int *item) { *item += 100; }
void proc_3(int &item) { item += 100; }
int main() {
    int value = 5;
    proc_1(value);
    std::cout << value << std::endl; // 5
    proc_2(&value);
    std::cout << value << std::endl; // 105
    proc_3(value);
    std::cout << value << std::endl; // 205
    return 0;
}